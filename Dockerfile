# Based on Dockerfile 
# http://gist.githubusercontent.com/SkypLabs/aa27e9f37471280c12d75e265a067d9e/raw/9f2895d713d39ea187f2bf82223a2c4b07487b4f/Dockerfile
# from http://blog.skyplabs.net/2017/08/29/angular-running-unit-tests-with-chromium-in-a-docker-container
FROM docker.io/node:8-stretch
MAINTAINER Oliver Carr <oli@ozoli.io>

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y chromium

ENV CHROME_BIN=chromium

WORKDIR /usr/src/app
CMD ["npm", "start"]
